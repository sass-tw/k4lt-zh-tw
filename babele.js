Hooks.once('init', () => {
    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'k4lt-zh-tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });        
    }
});

